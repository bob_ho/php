# Build Docker Image

docker build -t php .

# Running Image

docker run --name php -p 80:80 -t php

# Instrument Application

/opt/appdynamics/appdynamics-php-agent/install.sh -a=customer1@a95e63ab-35cd-4d10-9c82-b5ff822e161a localhost 8090 BusinessPortal BusinessPortal-Services BusinessPortal-WEB_NODE 