# Set the base image
FROM php:7.0-apache
# Dockerfile author / maintainer 
MAINTAINER Bob Ho <bob.ho@appdynamics.com> 

USER root

#Update and install Components
RUN apt-get update && apt-get install -y wget && apt-get install -y git
RUN docker-php-ext-install mysqli

#Install PHP Application

RUN git clone https://bob_ho@bitbucket.org/bob_ho/phpapp.git

# Download PHP Agent

RUN mkdir /opt/appdynamics/ && wget https://www.dropbox.com/s/2faznpgzckn3prj/appdynamics-php-agent-x64-linux-4.3.2.5.tar.bz2?dl=1 -O /opt/appdynamics/appdynamics-php-agent-x64-linux-4.3.2.5.tar.bz2
RUN chmod 755 /opt/appdynamics/appdynamics-php-agent-x64-linux-4.3.2.5.tar.bz2 && tar -xvjf /opt/appdynamics/appdynamics-php-agent-x64-linux-4.3.2.5.tar.bz2 -C /opt/appdynamics/